export default {
	mode: 'spa',
	/*
	 ** Headers of the page
	 */
	head: {
		title: process.env.npm_package_name || '',
		meta: [
			{charset: 'utf-8'},
			{name: 'viewport', content: 'width=device-width, initial-scale=1'},
			{
				hid: 'description',
				name: 'description',
				content:
					'Die Webseite liefert eine Übersicht über das BastiBattle (BaBa)'
			},
			{
				name: 'keywords',
				lang: 'de',
				content:
					'BastiGHG, BaBa, BastiBattle, Basti, Battle, Minecraft, PVE'
			},
		],
		link: [{rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'},{ rel: 'canonical', href: 'https://baba.wolves.ink' },],
		htmlAttrs: {
			lang: 'de'
		}
	},
	/*
	 ** Customize the progress-bar color
	 */
	loading: {color: '#fff'},
	/*
	 ** Global CSS
	 */
	css: [],
	/*
	 ** Plugins to load before mounting the App
	 */
	plugins: [],
	/*
	 ** Nuxt.js dev-modules
	 */
	buildModules: [],
	/*
	 ** Nuxt.js modules
	 */
	modules: [],
	/*
	 ** Build configuration
	 */
	build: {
		/*
		 ** You can extend webpack config here
		 */
		extend(config, ctx) {}
	}
}
